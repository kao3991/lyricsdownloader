using System;

namespace LyricsDownloaderGUI
{
	public class Configuration
	{

		private static Configuration instance = null;
		public static Configuration getInstance() {
			if (instance == null) {
				instance = new Configuration ();
			}
			return instance;
		}

		private Configuration ()
		{
		}

		public string lastFmLogin {
			get;
			set;
		}
		public int refreshDelay {
			get;
			set;
		}

		public void save() {
			//TODO zapisywanue
		}

	}
}

