using System;
using System.ComponentModel;

namespace LyricsDownloaderGUI
{
	public class LyricsGetter
	{
		static MonoTest.SongText st;
		public static void getLyricsForCurrentSong (object s, DoWorkEventArgs e)
		{
			MonoTest.LyricsDownloader.initialize (Configuration.getInstance().lastFmLogin);
			MonoTest.SongText temp_text = MonoTest.LyricsDownloader.getLyrics ();
			BackgroundWorker bw = s as BackgroundWorker;
			if (!bw.CancellationPending) { 
				st = temp_text;
			} else {
				MonoTest.Debug.echo ("Text found, but setting aborted.");
			}
		}
		static object Lock = new object();
		public static void setText(object s, RunWorkerCompletedEventArgs r) {
			lock (Lock) {
				BackgroundWorker bw = s as BackgroundWorker;
				if (!bw.CancellationPending) {
					if (st != null && st.text != null) {
						LDGUI.win.setText (st);
						MonoTest.Debug.echo ("Ustawiono tekst");
					} else {
						MonoTest.Debug.echo ("Ustawianie niełudane");
					}
				} else {
					MonoTest.Debug.echo ("Cancellation pending, not setting text");
				}
			}
		}

		public static void threadDisposed(object s, EventArgs r) {
			MonoTest.Debug.echo ("Thread disposed");
		}
	}
}

