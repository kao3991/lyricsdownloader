using System;
using Gtk;
using LyricsDownloaderGUI;

public partial class MainWindow: Gtk.Window
{	
	private string textHolder = "";

	[System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)]
	public void updateText() {
		//lock (Lock) { 
			this.LyricsBox.Buffer.Text = textHolder;
		//}
	}

	public void updateStatus (string status) {
		this.statusbar.Push(0, status);
	}
	public MainWindow (): base (Gtk.WindowType.Toplevel)
	{
		Build ();
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}
	static object Lock = new object();

	public void setText(string text, bool status = true) {
		if (status) {
			this.updateStatus ("Pobrano tekst");
		}
		this.RefreshButton.Sensitive = true;
		this.textHolder = text;
		this.updateText ();
	}

	public void setText(MonoTest.SongText st) {
		this.setText (st.text, false);
		this.updateStatus (st.song.artist + "-" + st.song.title + " (z "+st.source+")");
	}

	protected void getCurrentSongText (object sender, EventArgs e)
	{
		MonoTest.Debug.echo ("Start getting...");
		this.updateStatus("Pobieranie tekstu...");
		this.RefreshButton.Sensitive = false;
		LyricsDownloaderGUI.LDGUI.getCurrentSongText ();
	}
	/*
	protected void getCurrentSongText (object sender, EventArgs e)
	{
		throw new NotImplementedException ();
	}
	*/
	public bool AllowShrink {
		get;
		set;
	}

	protected void OnRefreshButtonClicked (object sender, EventArgs e)
	{
		getCurrentSongText (sender, e);
	}


	protected void OnUstawieniaActionActivated (object sender, EventArgs e)
	{
		new SettingsWindow ();
	}
}
