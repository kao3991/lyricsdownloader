using System;
using Gtk;
using System.ComponentModel;

namespace LyricsDownloaderGUI
{
	class LDGUI
	{
		public static MainWindow win;
		public static void Main (string[] args)
		{
			Application.Init ();
			win = new MainWindow ();
			win.Show ();
			Application.Run ();
		}

		public static BackgroundWorker getCurrentSongText() {
			BackgroundWorker bw = new BackgroundWorker ();
			bw.WorkerSupportsCancellation = true;
			bw.DoWork += LyricsGetter.getLyricsForCurrentSong;
			bw.RunWorkerCompleted += LyricsGetter.setText;
			bw.Disposed += LyricsGetter.threadDisposed;
			bw.RunWorkerAsync ();
			return bw;
		}
	}
}
