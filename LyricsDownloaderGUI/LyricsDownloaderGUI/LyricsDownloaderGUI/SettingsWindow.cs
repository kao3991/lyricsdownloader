using System;

namespace LyricsDownloaderGUI
{
	public partial class SettingsWindow : Gtk.Window
	{
		public SettingsWindow () : 
				base(Gtk.WindowType.Toplevel)
		{
			this.Build ();
			var c = Configuration.getInstance ();
			this.lastFmLoginEntry.Text = c.lastFmLogin;
			this.refreshDelayEntry.Text = Convert.ToString(c.refreshDelay);
		}

		protected void OnSaveSettingsButtonClicked (object sender, EventArgs e)
		{
			var c = Configuration.getInstance ();
			c.lastFmLogin = this.lastFmLoginEntry.Text;
			try {
				c.refreshDelay = Convert.ToInt32(refreshDelayEntry.Text);
			} catch (Exception ex) {
				//TODO obsługa tego wyjątku :P 
			}
			c.save ();
		}
	}
}

