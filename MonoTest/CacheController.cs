using System.IO;

namespace MonoTest {
	class CacheController {
		public static string CACHE_DIRECTORY = "/home/krzysiek/.Texts/";
		public static char DIRECTORY_SEPARATOR = '/';
		protected void saveCache (string path, string content, bool createIfNotExists = true)
		{
			if (createIfNotExists && !File.Exists(this.preparePath(path))) {
				this.CreateNecessaryDirectories(path);
			}
			//File.Create(this.preparePath(CACHE_DIRECTORY + path));
			File.WriteAllText(this.preparePath( CACHE_DIRECTORY + path), content);
		}
		protected string getCached (string path) {
		if (File.Exists (this.preparePath ( CACHE_DIRECTORY + path))) {
				string text = File.ReadAllText (this.preparePath (  CACHE_DIRECTORY + path));
				return text;
			} else {
				return null;
			}
		}
		protected string preparePath(string path) {
			return path;
		}

		protected void CreateNecessaryDirectories (string path, bool ommitLast = true) {
			System.Collections.Generic.List<string> pathSegments = new System.Collections.Generic.List<string>(path.Split (DIRECTORY_SEPARATOR));
			pathSegments.RemoveAt(pathSegments.Count - 1);
			string pathCreated = "";
			foreach (string segment in pathSegments) {
				string currentPath = CACHE_DIRECTORY + pathCreated + segment;
				if (!Directory.Exists(currentPath)) {
						System.Console.WriteLine(currentPath);
						Directory.CreateDirectory(currentPath);
				}
				pathCreated = pathCreated + DIRECTORY_SEPARATOR + segment;
			}
		}
	}
}