namespace MonoTest {
	class CacheTextGetter:TextGetter {
		new public bool enableCaching = false;
		override public SongText getTextForSong(Song song) {
			var st =  new SongTextCache().getCachedText(song);
			st.source = "cache";
			return st;
		}
	}
}