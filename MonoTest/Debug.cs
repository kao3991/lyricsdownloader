namespace MonoTest {
	public class Debug {
		private static bool debugMode = false;
		public static void echo (string text) {
			if (debugMode) {
				System.Console.WriteLine(text);
			}
		}
	}
}