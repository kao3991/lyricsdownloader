using System;
using System.Net;
namespace LastFmClient {
	public class LastFmClient {
		private String apiKey = "a2411e2ab866ea5c6ab93c1614557018";
		private String url = "http://ws.audioscrobbler.com/2.0/";

		internal LastFmClient () {
		}

		public LastFmResponse request(LastFmRequest request) {
			return request.getResponse();
		}

		public LastFmRequest createRequest() {
			LastFmRequest rq = new LastFmRequest(url);
			rq.setParam("api_key", this.apiKey);
			return rq;
		}
	}
}

