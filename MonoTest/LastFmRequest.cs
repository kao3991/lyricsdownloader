using System;
using System.Collections.Specialized;

namespace LastFmClient {
	public class LastFmRequest {
		public string url {get;set;}
		public LastFmRequest(String url):this() {
			this.url = url;
		}
		public LastFmRequest() {
			parameters = new NameValueCollection();
		}

		private NameValueCollection parameters;
		public void setParam(String key, String value) {
			parameters.Add(key, value);
		}

		public NameValueCollection getParams() {
			return parameters;
		}

		public LastFmResponse getResponse() {
			WebClient wc = new WebClient();
			System.Collections.Specialized.NameValueCollection p = this.getParams();
			string uri = this.getUrl();
			String response = wc.DownloadString(uri);
			return new LastFmResponse(response);
		}
		private string getUrl () {
			var sb = new System.Text.StringBuilder(this.url);
			sb.Append("?");
			foreach (string key in this.parameters) {
				foreach (string val in this.parameters.GetValues(key)) {
					sb.AppendFormat("{0}={1}&", key, val);
				}
			}
			return sb.ToString();

		}
	}
}

