using System;
using System.Xml.XPath;
using System.IO;
namespace LastFmClient {
	public class LastFmResponse {
		private XPathDocument content { set; get; }
		public LastFmResponse(String xmlContent) {
			XPathDocument xpd;
			using (StringReader sr = new StringReader(xmlContent)) {
				xpd = new XPathDocument(sr);
			}
			content = xpd;
		}
		public XPathDocument getContent(){
			return this.content;
		}
	}
}

