using System;
using System.Xml.XPath;
namespace MonoTest {
	class LastFmSongGetter : CurrentSongGetter {
		public LastFmSongGetter (String username) {
			this.username = username;
		}
		public String username;
		public override Song getCurrentSong() {
			LastFmClient.LastFmClient lfc = new LastFmClient.LastFmClient();
			LastFmClient.LastFmRequest rq = lfc.createRequest();
			rq.setParam("user", username);
			rq.setParam("method", "user.getrecenttracks");
			rq.setParam ("limit", "1");
			LastFmClient.LastFmResponse res = lfc.request(rq);
			XPathNavigator xpn = res.getContent ().CreateNavigator ();
			XPathNodeIterator iterator = xpn.Select ("lfm/recenttracks[1]/track[1]/artist");
			iterator.MoveNext ();
			string autor = iterator.Current.Value;
			iterator = xpn.Select ("lfm/recenttracks[1]/track[1]/name");
			iterator.MoveNext ();
			string title = iterator.Current.Value;
			return new Song (title, autor);
		}
	}
}

