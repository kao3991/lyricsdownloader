using System;
namespace MonoTest {
	public class LyricsDownloader
	{
		private static String lastfm_username;
		static void Main (string[] args) {
			System.Console.WriteLine (getLyrics ().text);
		}
		public static void initialize(String lastfmUsername) {
			lastfm_username = lastfmUsername;
		}
		public static SongText getLyrics() {
			Song s = new LastFmSongGetter(lastfm_username).getCurrentSong ();
			SongText st = TextGetter.getText (s);
			return st;
		}
	}
}
