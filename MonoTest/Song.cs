namespace MonoTest {
	public class Song {
		public Song(string title, string artist) {
			this.title = title;
			this.artist = artist;
		}
		public Song() {
		}
		public string title {get; set;}
		public string artist {get; set;}
	}
}
