namespace MonoTest {
	public class SongText {
		public SongText(Song song, string text) {
			this.text = text;
			this.song = song;
		}
		public Song song {
			get; set;
		}
		public string text {
			get;
			set;
		}
		public string source {
			get;
			set;
		}
		public void saveToCache() {
			new SongTextCache().cacheText(this); 
		}

		public bool isValid() {
			return (this.text != null && this.text != null && this.text.Length > 1);
		}
	}
}