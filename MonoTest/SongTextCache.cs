namespace MonoTest {
	class SongTextCache:CacheController {
		public void cacheText(SongText st) {
			string path = getPathForSong(st.song);
			this.saveCache(path, st.text, true);
		}

		public SongText getCachedText(Song s) {
			string text = this.getCached(this.getPathForSong(s));
			return new SongText(s, text);
		}

		private string getPathForSong(Song s) {
			string path = s.artist + CacheController.DIRECTORY_SEPARATOR + s.title + ".txt";
			return path;
		}
	}
}