using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Xml.XPath;

namespace MonoTest {
	class TekstowoTextGetter:TextGetter
	{

		public override SongText getTextForSong (Song song) {
			string url = this.prepareUrl (song);
			Debug.echo(url);
			WebClient wc = new WebClient ();
			SongText st = null;
			try {
				st = new SongText (song, this.extractTextFromHTML (wc.DownloadString (url)));
			} catch (System.Exception e) {
				Debug.echo ("attempt to find text...");
				return this.findTextForSong (song);
			}
			st.source = "tekstowo.pl";
			return st;
		}

		protected String prepareUrl(Song song) {
//			Dictionary<string,string> replaces = new Dictionary<string,string>();
//			replaces.Add(" ", "_");
//			replaces.Add("ł", "l");
//			replaces.Add("ó", "o");
//			replaces.Add("Я", "r");
//			replaces.Add("Z", "r");
			String url = "http://tekstowo.pl/piosenka," + Tools.normalizeString(this.prepareAutor(song.artist)) + "," + Tools.normalizeString(this.prepareTitle(song.title)) + ".html";
			return url;
		}

		protected String prepareSearchUrl(Song song) {
			String url = "http://www.tekstowo.pl/szukaj,wykonawca,"+Tools.normalizeStringToUrl(this.prepareAutor(song.artist))+",tytul,"+Tools.normalizeStringToUrl(this.prepareTitle(song.title))+".html";
			return url;
		}
		protected String prepareAutor(string autor) {
			return autor.ToLower();
		}
		protected String prepareTitle(string title) {
			return title.ToLower();
		}
		protected String extractTextFromHTML(string html) {
			String text = "";
			String beginning = "<div class=\"song-text\">";
			String end = "<a href=\"javascript:;\" id=\"song_revisions_link";
			text = html.Substring(
				html.IndexOf(
					beginning
				), 
				html.IndexOf(
					end, 
					html.IndexOf(
						beginning
					)
				)- html.IndexOf(beginning)
			);
			text = text.Replace("<h2>Tekst piosenki:</h2>", "");
			text = Regex.Replace(text, "<[^>].*>", "");
			text = text.Trim();
			return text;
		}

		protected SongText findTextForSong(Song song) {
			WebClient wc = new WebClient ();
			string searchUrl = this.prepareSearchUrl (song);
			Debug.echo ("Search url: " + searchUrl);
			string html = wc.DownloadString (searchUrl);
			html = html.Substring (html.IndexOf ("<div class=\"content\">"));
			html = html.Substring (html.IndexOf ("<div class=\"box-przeboje\">"));
			html = html.Substring(0, html.IndexOf("</div>"));
			html = html.Substring (html.IndexOf ("<a href="), html.IndexOf ("</a") - html.IndexOf ("<a href="));
			html = html.Substring (html.IndexOf("\"")+1);
			html = html.Substring (0, html.IndexOf ("\""));
			Debug.echo(html);
			string href = html;
			Debug.echo ("Found! url:" + href);
			string text = this.extractTextFromHTML (wc.DownloadString ("http://tekstowo.pl" + href));
			Debug.echo (text);
			return new SongText (song, text);
		}
	}
}