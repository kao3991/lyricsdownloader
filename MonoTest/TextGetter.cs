using System.Collections.Generic;

namespace MonoTest {
	abstract class TextGetter {

		public static SongText getText(Song song, bool saveInCache = true) {
			List<TextGetter> getters = new List<TextGetter> ();
			getters.Add (new CacheTextGetter ());
			getters.Add (new TekstowoTextGetter ());
			SongText st = null;
			foreach (TextGetter tg in getters) {
				try {
					st = tg.getTextForSong(song);
					if (st !=  null && st.isValid()) {
						if (saveInCache && tg.enableCaching) {
							st.saveToCache();
						}
						break;
					}
				} catch (System.Exception e) {
					Debug.echo("Error getting text: " + e.Message);
				}
			}
			return st;
		}
		public bool enableCaching = true;
		abstract public SongText getTextForSong(Song song);
	}
}
