using System.Collections.Generic;
namespace MonoTest {
	class Tools {
		public static string DicionaryReplace(string input, Dictionary<string, string> replaces) {
			foreach (KeyValuePair<string, string> pair in replaces) {
				input = input.Replace(pair.Key, pair.Value);
			}
			return input;
		}

		public static string normalizeString(string input) {
			Dictionary<string,string> replaces = new Dictionary<string,string>();
			replaces.Add(" ", "_");
			replaces.Add("ł", "l");
			replaces.Add("ó", "o");
			replaces.Add("Я", "r");
			replaces.Add("ą", "a");
			replaces.Add("ć", "c");
			replaces.Add("ę", "e");
			replaces.Add("ń", "n");
			replaces.Add("ś", "s");
			replaces.Add("ż", "z");
			replaces.Add("ź", "z");
			return DicionaryReplace(input, replaces).ToLower();
		}

		public static string normalizeStringToUrl(string input) {
			Dictionary<string,string> replaces = new Dictionary<string,string>();
			replaces.Add(" ", "+");
			replaces.Add("ł", "l");
			replaces.Add("ó", "o");
			replaces.Add("Я", "r");
			replaces.Add("ą", "a");
			replaces.Add("ć", "c");
			replaces.Add("ę", "e");
			replaces.Add("ń", "n");
			replaces.Add("ś", "s");
			replaces.Add("ż", "z");
			replaces.Add("ź", "z");
			return DicionaryReplace(input, replaces).ToLower();
		}
	}
}
