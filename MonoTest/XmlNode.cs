using System;

namespace LastFmClient
{
	public class XmlNode<T>
	{
		private System.Collections.Hashtable attrs;
		private T value;
		private XmlNode(){
			this.attrs = new System.Collections.Hashtable();
		}

		public void setAttr(string key, string value){
			this.attrs.Add (key, value);
		}
		public string getAttr(string key) {
			return this.attrs [key].ToString ();
		}
	}
}

